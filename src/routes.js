// aqui não preciso desta forma importar todo express, mas somente o Router
const { Router } = require('express');

const routes = new Router();

routes.get('/test', (req, res) => {
    return res.json ({ message: 'OK!' });
});

module.exports = routes;
